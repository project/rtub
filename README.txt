README.txt for Real Time User Behaviour module
---------------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration

INTRODUCTION
------------

This module tracks each user event like User Login, User Logout
and User Nodeview events and also store these event data in your database.
This data is then exposed to views so you can report on it if required.

Note: Front Page/Home Page be tracked by default.
We dont track Node view event for anonymous user.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

RECOMMENDED MODULES
-------------------

Markdown filter (https://www.drupal.org/project/markdown):
When enabled, display of the project's README.md help will be rendered
with markdown.

INSTALLATION
------------

 - Install the rtub module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
------------

* Configure the user permissions in Administration » People » Permissions:

If you wish to allow certain roles from being tracked, then you can assign the
following permission to track Login and Logout event for specific roles:
  - User login tracking
  - User logout tracking

For NodeView Event tracking

If you wish to track user nodeview event for certain content type only,
then you can assign permission to track nodeView event for specific
content type for specific user role.

Views integration

You can access the data in the real time user behaviour tables either as a
base view, or more usefully as a relationship against the user table.
