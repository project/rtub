/**
 * @file
 * rtub: NodeView Ajax call functionality.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  var initialized;

  function init() {
    if (!initialized) {
      initialized = true;
      var eData = drupalSettings.rtub.nodeview.eventData;
      var eType = drupalSettings.rtub.nodeview.eventType;
      var eToken = drupalSettings.rtub.nodeview.eventToken;
      var newNode = {event: {eData: eData, eType: eType, eToken: eToken}};
      postNode(newNode);
    }
  }

  function postNode(node) {
    jQuery.ajax({
      url: Drupal.url('rtub/post'),
      type: 'POST',
      data: node,
      dataType: 'json'
    });
  }
  Drupal.behaviors.rtub_ct = {
    attach: function (context) {
      init();
    }
  };
})(jQuery, Drupal, drupalSettings);
