<?php

/**
 * @file
 * Views integration for the Real Time User Behaviour module.
 */

/**
 * Implements hook_views_data().
 */
function rtub_views_data() {
  $data = [];
  $data['rtub']['table']['group'] = t('Real Time User behaviour');
  $data['rtub']['table']['base'] = [
    'field' => 'id',
    'title' => t('Real Time User Events'),
    'help'  => t('Contains data about user user events.'),
  ];
  $data['login_tracker']['table']['join'] = [
    'users_field_data' => [
      'left_field' => 'uid',
      'field'      => 'uid',
    ],
  ];
  // The UID field.
  $data['rtub']['id'] = [
    'title' => t('Event record ID'),
    'help'  => t("The unique ID referring to this event record."),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'title' => t('Event ID'),
      'id' => 'numeric',
    ],
  ];
  $data['rtub']['uid'] = [
    'title' => t('UID'),
    'help'  => t("The user's UID."),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'title' => t('UID'),
      'id' => 'numeric',
    ],
    'relationship' => [
      'title'              => t('User who logged in'),
      'help'               => t('The user associated with the login record.'),
      'id'                 => 'standard',
      'base'               => 'users_field_data',
      'base field'         => 'uid',
      'field'              => 'uid',
      'label'              => t('User who logged in'),
    ],
  ];
  // Event Type field.
  $data['rtub']['event_type'] = [
    'title' => t('Event Type'),
    'help'  => t('Type Of Events.'),
    'field' => [
      'id'             => 'standard',
      'click sortable' => FALSE,
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];
  // The login timestamp field.
  $data['rtub']['event_timestamp'] = [
    'title'  => t('Event timestamp'),
    'help'   => t('The timestamp that the user events in at.'),
    'field'  => [
      'id' => 'date',
    ],
    'sort'   => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
  ];
  $data['rtub']['event_data'] = [
    'title' => t('Event data'),
    'help'  => t('Event information.'),
    'field' => [
      'id'             => 'standard',
      'click sortable' => FALSE,
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];
  return $data;
}
