<?php

namespace Drupal\rtub\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Drupal\Core\Database\Connection;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Site\Settings;
use Drupal\Component\Utility\Crypt;

/**
 * Controller for the comment entity.
 *
 * @see \Drupal\comment\Entity\Comment.
 */
class PostNodeDataController extends ControllerBase {

  /**
   * The HTTP kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $connection;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a CommentController object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   HTTP kernel to handle requests.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(HttpKernelInterface $http_kernel, Connection $connection, TimeInterface $time) {
    $this->httpKernel = $http_kernel;
    $this->connection = $connection;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_kernel'),
      $container->get('database'),
      $container->get('datetime.time')
    );
  }

  /**
   * Returns a set of nodes' last read timestamps.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request of the page.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function updateRealTimeData(Request $request) {

    if ($this->currentUser()->isAnonymous()) {
      throw new AccessDeniedHttpException();
    }
    $event = $request->request->get('event');
    $title = $event['eData'];
    $type = $event['eType'];
    $newStr = "User on {$title} | {$type}";
    $eStr = Crypt::hmacBase64($newStr, Settings::getHashSalt());

    if (hash_equals($eStr, $event['eToken'])) {
      // Valid post request.
      $connection = $this->connection;
      $connection->insert('rtub')
        ->fields([
          'uid' => $this->currentUser()->id(),
          'event_type' => $type,
          'event_timestamp' => $this->time->getRequestTime(),
          'event_data' => "User on {$title} Page",
        ])->execute();

      $response['status'] = TRUE;
      return new JsonResponse($response);
    }
    else {
      throw new NotFoundHttpException();
    }
  }

}
